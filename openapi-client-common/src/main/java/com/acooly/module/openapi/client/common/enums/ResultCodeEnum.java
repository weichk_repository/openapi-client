package com.acooly.module.openapi.client.common.enums;

public enum ResultCodeEnum {

	/** 执行成功 */
	EXECUTE_SUCCESS("EXECUTE_SUCCESS", "成功"),

	/** 执行处理中 */
	EXECUTE_PROCESSING("EXECUTE_PROCESSING", "处理中"),

	/** 执行失败 */
	EXECUTE_FAIL("EXECUTE_FAIL", "失败");

	private String code;
	private String message;

	ResultCodeEnum(String code, String message) {
		this.code = code;
		this.message = message;
	}

	public String getCode() {
		return code;
	}

	public String code() {
		return code;
	}

	public String message() {
		return message;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
