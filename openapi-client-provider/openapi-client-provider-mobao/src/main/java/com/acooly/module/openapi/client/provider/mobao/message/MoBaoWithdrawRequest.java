/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 跳转提现 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoWithdrawRequest extends MoBaoRequest {

    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo = Ids.getDid();

    @NotEmpty
    @Size(max = 32)
    @XStreamAlias("UserID")
    private String userId;

    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    @NotEmpty
    @XStreamAlias("Commission")
    private String commission;

    @XStreamAlias("CustParam")
    private String custParam;

    @NotEmpty
    @Size(max = 20)
    @XStreamAlias("CustomerIP")
    private String customerIP; // = Constants.SITE_IP;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("ReturnUrl")
    private String returnUrl; // = Constants.WITHDRAW_RETRUE_URL;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("NotifyUrl")
    private String notifyUrl; // = Constants.NOTIFY_URL + "/CarryOut";

    @Size(max = 120)
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "提现";

    public MoBaoWithdrawRequest() {
        setMessageType("CarryOut");
    }

    public MoBaoWithdrawRequest(String userId, String tradeAmt, String commission) {
        this();
        this.userId = userId;
        this.tradeAmt = tradeAmt;
        this.commission = commission;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    public String getCustomerIP() {
        return customerIP;
    }

    public void setCustomerIP(String customerIP) {
        this.customerIP = customerIP;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoDepositRequest: {orderNo:%s, userId:%s, tradeAmt:%s, commission:%s, custParam:%s, customerIP:%s, returnUrl:%s, notifyUrl:%s, tradeSummary:%s}",
                        orderNo, userId, tradeAmt, commission, custParam, customerIP, returnUrl, notifyUrl,
                        tradeSummary);
    }

}
