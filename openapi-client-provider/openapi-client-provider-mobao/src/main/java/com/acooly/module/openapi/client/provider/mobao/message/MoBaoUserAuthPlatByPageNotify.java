/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao.message;


import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 2.1 授权签约 异步通知
 * 
 * @author zhangpu
 * 
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoUserAuthPlatByPageNotify extends MoBaoNotify {

	@NotEmpty
	@XStreamAlias("UserID")
	private String userId;

	@XStreamAlias("CustParam")
	private String custParam;

	/** yyyyMMddHHmmss */
	@NotEmpty
	@XStreamAlias("TradeTime")
	private String tradeTime;

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getCustParam() {
		return custParam;
	}

	public void setCustParam(String custParam) {
		this.custParam = custParam;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	@Override
	public String toString() {
		return String.format("MoBaoUserAuthPlatByPageNotify: {userId:%s, custParam:%s, tradeTime:%s}", userId,
				custParam, tradeTime);
	}

}
