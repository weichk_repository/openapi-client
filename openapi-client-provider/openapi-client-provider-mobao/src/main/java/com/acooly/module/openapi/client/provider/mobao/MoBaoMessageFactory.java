/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao;


import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.api.message.MessageMeta;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.acooly.module.openapi.client.provider.mobao.message.*;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author zhangpu
 */
public class MoBaoMessageFactory implements MessageFactory {

	public static Map<String, MessageMeta> metas = Maps.newHashMap();

	static {
		/** 网银充值 */
		metas.put(MoBaoServiceKeys.Deposit.getKey(), new MessageMeta(null, MoBaoDepositNotify.class));
		/** 跳转提现 */
		metas.put(MoBaoServiceKeys.Withdraw.getKey(), new MessageMeta(null, MoBaoWithdrawNotify.class));
		/** 跳转签约 */
		metas.put(MoBaoServiceKeys.Sign.getKey(), new MessageMeta(null, MoBaoUserAuthPlatByPageNotify.class));
		/** 投资单笔查询 */
		metas.put(MoBaoServiceKeys.InvestQuery.getKey(), new MessageMeta(MoBaoOrderPayQueryResponse.class));
		/** 投资单笔查询 */
		metas.put(MoBaoServiceKeys.TenderQuery.getKey(), new MessageMeta(MoBaoOrderQueryResponse.class));
		/** 红包单笔查询 */
		metas.put(MoBaoServiceKeys.BonusQuery.getKey(), new MessageMeta(MoBaoAcctTradeQueryResponse.class));
		/** 充提单笔查询 */
		metas.put(MoBaoServiceKeys.CarryOutQuery.getKey(), new MessageMeta(MoBaoCarryOutQueryResponse.class));
	}


	@Override
	public ApiMessage getRequest(String serviceName) {
		return null;
	}

	@Override
	public ApiMessage getResponse(String serviceName) {
		if (metas.get(serviceName) == null || metas.get(serviceName).getResponse() == null) {
			return newInstance(MoBaoResponse.class);
		} else {
			return newInstance(metas.get(serviceName).getResponse());
		}
	}

	@Override
	public ApiMessage getNotify(String serviceName) {
		return newInstance(metas.get(serviceName).getAsyncNotify());
	}

	@Override
	public ApiMessage getReturn(String serviceName) {
		return newInstance(metas.get(serviceName).getResponse());
	}

	public <T> T newInstance(Class<T> clazz) {
		try {
			return (T) clazz.newInstance();
		} catch (InstantiationException e) {
			throw new RuntimeException("InstantiationException:" + clazz);
		} catch (IllegalAccessException e) {
			throw new RuntimeException("IllegalAccessException:" + clazz);
		}
	}
}
