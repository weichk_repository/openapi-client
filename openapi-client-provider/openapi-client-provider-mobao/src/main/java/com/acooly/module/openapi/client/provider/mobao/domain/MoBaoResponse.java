/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.mobao.domain;

import com.acooly.module.openapi.client.api.message.DefaultApiMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoResponse extends DefaultApiMessage {

    @XStreamAlias("MessageType")
    @XStreamAsAttribute
    private String messageType;

    @XStreamAlias("RespCode")
    private String respCode;

    @XStreamAlias("RespDesc")
    private String respDesc;

    @XStreamAlias("RespDetail")
    private String respDetail;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getRespCode() {
        return respCode;
    }

    public void setRespCode(String respCode) {
        this.respCode = respCode;
    }

    public String getRespDesc() {
        return respDesc;
    }

    public void setRespDesc(String respDesc) {
        this.respDesc = respDesc;
    }

    public String getRespDetail() {
        return respDetail;
    }

    public void setRespDetail(String respDetail) {
        this.respDetail = respDetail;
    }

    @Override
    public String getService() {
        return this.messageType;
    }

    @Override
    public String toString() {
        return String.format("MoBaoResponse: {messageType:%s, respCode:%s, respDesc:%s, respDetail:%s}", messageType,
                respCode, respDesc, respDetail);
    }

}
