/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.mobao.domain;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author zhangpu
 */
public class MoBaoRequest implements ApiMessage {

    @XStreamAlias("MessageType")
    @XStreamAsAttribute
    private String messageType;

    @XStreamAlias("PlatformID")
    @XStreamAsAttribute
    private String platformID;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    public String getPlatformID() {
        return platformID;
    }

    public void setPlatformID(String platformID) {
        this.platformID = platformID;
    }

    @Override
    public String getService() {
        return this.messageType;
    }

    @Override
    public String getPartner() {
        return this.platformID;
    }

}
