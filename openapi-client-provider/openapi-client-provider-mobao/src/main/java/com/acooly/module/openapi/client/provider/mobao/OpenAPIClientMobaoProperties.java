/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-16 16:11 创建
 */
package com.acooly.module.openapi.client.provider.mobao;

import com.acooly.core.utils.Strings;
import org.springframework.boot.context.properties.ConfigurationProperties;

import static com.acooly.module.openapi.client.provider.mobao.OpenAPIClientMobaoProperties.PREFIX;


/**
 * @author zhangpu@acooly.cn
 */
@ConfigurationProperties(prefix = PREFIX)
public class OpenAPIClientMobaoProperties {
    public static final String PREFIX = "acooly.openapi.client.mobao";


    /**
     * 商户号
     */
    private String partnerId;

    /**
     * 连接超时时间（毫秒）
     */
    private long connTimeout = 10000;

    /**
     * 读超时时间（毫秒）
     */
    private long readTimeout = 30000;
    /**
     * 网关地址
     */
    private String gateway;

    /**
     * 网关跳转地址
     */
    private String gatewayWeb;

    /**
     * 秘钥库
     */
    private String keystore;

    /**
     * 秘钥库保护密码
     */
    private String keystorePassword;

    private String cert;
    /**
     * 本系统域名（用于自动生成回调地址）
     */
    private String domain;

    private String ip;

    /**
     * 异步通知URL
     */
    private String notifyUrl;
    /**
     * WEB充值同步通知地址
     */
    private String depositReturnUrl;
    /**
     * WEB提现同步通知地址
     */
    private String withdrawRerurnUrl;
    /**
     * 投资同步通知地址
     */
    private String investRerurnUrl;

    /**
     * 签约同步通知地址
     */
    private String signRerurnUrl;


    public String getNotifyUrl(String service) {
        return getCanonicalUrl(this.domain, this.notifyUrl);
    }

    public String getReturnUrl(String url) {
        return getCanonicalUrl(this.domain, url);
    }


    protected String getCanonicalUrl(String prefix, String postfix) {
        if (Strings.endsWith(prefix, "/")) {
            prefix = Strings.removeEnd(prefix, "/");
        }
        if (Strings.startsWith(postfix, "/")) {
            return prefix + postfix;
        } else {
            return prefix + "/" + postfix;
        }
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public long getConnTimeout() {
        return connTimeout;
    }

    public void setConnTimeout(long connTimeout) {
        this.connTimeout = connTimeout;
    }

    public long getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(long readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getGateway() {
        return gateway;
    }

    public void setGateway(String gateway) {
        this.gateway = gateway;
    }

    public String getGatewayWeb() {
        return gatewayWeb;
    }

    public void setGatewayWeb(String gatewayWeb) {
        this.gatewayWeb = gatewayWeb;
    }

    public String getKeystore() {
        return keystore;
    }

    public void setKeystore(String keystore) {
        this.keystore = keystore;
    }

    public String getKeystorePassword() {
        return keystorePassword;
    }

    public void setKeystorePassword(String keystorePassword) {
        this.keystorePassword = keystorePassword;
    }

    public String getCert() {
        return cert;
    }

    public void setCert(String cert) {
        this.cert = cert;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getDepositReturnUrl() {
        return depositReturnUrl;
    }

    public void setDepositReturnUrl(String depositReturnUrl) {
        this.depositReturnUrl = depositReturnUrl;
    }

    public String getWithdrawRerurnUrl() {
        return withdrawRerurnUrl;
    }

    public void setWithdrawRerurnUrl(String withdrawRerurnUrl) {
        this.withdrawRerurnUrl = withdrawRerurnUrl;
    }

    public String getInvestRerurnUrl() {
        return investRerurnUrl;
    }

    public void setInvestRerurnUrl(String investRerurnUrl) {
        this.investRerurnUrl = investRerurnUrl;
    }

    public String getSignRerurnUrl() {
        return signRerurnUrl;
    }

    public void setSignRerurnUrl(String signRerurnUrl) {
        this.signRerurnUrl = signRerurnUrl;
    }
}
