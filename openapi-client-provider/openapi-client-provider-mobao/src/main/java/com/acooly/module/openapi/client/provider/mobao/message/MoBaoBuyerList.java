/**
 * create by zhangpu
 * date:2015年3月22日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.api.util.XStreams;
import com.google.common.collect.Lists;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import org.hibernate.validator.constraints.NotEmpty;

import java.util.List;

/**
 * @author zhangpu
 *
 */
@XStreamAlias("BuyerList")
public class MoBaoBuyerList {
	/**
	 * 还款明细
	 */
	@NotEmpty
	@XStreamImplicit
	private List<MoBaoBuyer> buyerList = Lists.newArrayList();

	/** 明细数量 */
	@NotEmpty
	@XStreamAlias("Count")
	@XStreamAsAttribute
	private String count = String.valueOf(buyerList.size());

	public void addBuyer(String buyerId, String amt) {
		buyerList.add(new MoBaoBuyer(buyerId, amt));
		count = String.valueOf(buyerList.size());
	}

	public List<MoBaoBuyer> getBuyerList() {
		return buyerList;
	}

	public void setBuyerList(List<MoBaoBuyer> buyerList) {
		this.buyerList = buyerList;
	}

	public String getCount() {
		return this.count;
	}

	public void setCount(String count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return String.format("{buyerList:%s, count:%s}", buyerList, count);
	}

	public static void main(String[] args) {
		MoBaoBuyerList list = new MoBaoBuyerList();
		list.addBuyer("zhangpu1", "1000.00");
		list.addBuyer("zhangpu2", "2000.00");
		list.addBuyer("zhangpu3", "3000.00");

		System.out.println(XStreams.getXStream().toXML(list));
	}

}
