/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 4.4 用户投资查询 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayQueryRequest extends MoBaoRequest {

	/** 标的号 */
	@NotEmpty
	@XStreamAlias("OrderNo")
	private String orderNo = Ids.getDid();;

	/***
	 * 投资流水号，需唯一，可以由字母和数字组成。
	 */
	@NotEmpty
	@XStreamAlias("TradeNo")
	private String TradeNo;

	public MoBaoOrderPayQueryRequest() {
		setMessageType("OrderPayQuery");
	}

	public MoBaoOrderPayQueryRequest(String orderNo, String tradeNo) {
		this();
		this.orderNo = orderNo;
		TradeNo = tradeNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return TradeNo;
	}

	public void setTradeNo(String tradeNo) {
		TradeNo = tradeNo;
	}

	@Override
	public String toString() {
		return String.format("MoBaoOrderPayQueryRequest: {orderNo:%s, TradeNo:%s}", orderNo, TradeNo);
	}

}
