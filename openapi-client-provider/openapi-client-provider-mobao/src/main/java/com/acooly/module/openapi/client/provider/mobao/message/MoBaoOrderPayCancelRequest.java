/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.8 投资撤销 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayCancelRequest extends MoBaoRequest {

	/** 标的号 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("OrderNo")
	private String orderNo = Ids.getDid();;

	/***
	 * 投资流水号，需唯一，可以由字母和数字组成。
	 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("TradeNo")
	private String tradeNo = Ids.getDid();

	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	@NotEmpty
	@XStreamAlias("BuyerID")
	private String buyerID;

	@NotEmpty
	@XStreamAlias("SellerID")
	private String sellerID;

	@NotEmpty
	@XStreamAlias("TradeSummary")
	private String tradeSummary = "投资取消";

	public MoBaoOrderPayCancelRequest() {
		setMessageType("OrderPayCancel");
	}

	public MoBaoOrderPayCancelRequest(String orderNo, String tradeNo, String tradeAmt, String buyerID, String sellerID) {
		this();
		this.orderNo = orderNo;
		this.tradeNo = tradeNo;
		this.tradeAmt = tradeAmt;
		this.buyerID = buyerID;
		this.sellerID = sellerID;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return tradeNo;
	}

	public void setTradeNo(String tradeNo) {
		this.tradeNo = tradeNo;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getBuyerID() {
		return buyerID;
	}

	public void setBuyerID(String buyerID) {
		this.buyerID = buyerID;
	}

	public String getSellerID() {
		return sellerID;
	}

	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}

	public String getTradeSummary() {
		return tradeSummary;
	}

	public void setTradeSummary(String tradeSummary) {
		this.tradeSummary = tradeSummary;
	}

	@Override
	public String toString() {
		return String
				.format("MoBaoOrderPayCancelRequest: {orderNo:%s, tradeNo:%s, tradeAmt:%s, buyerID:%s, sellerID:%s, tradeSummary:%s}",
						orderNo, tradeNo, tradeAmt, buyerID, sellerID, tradeSummary);
	}

}
