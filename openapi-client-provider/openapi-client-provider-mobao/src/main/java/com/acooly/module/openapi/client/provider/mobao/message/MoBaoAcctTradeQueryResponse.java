/**
 * create by zhangpu
 * date:2015年3月25日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 3.12 红包缴费查询 响应报文
 *
 * @author zhangpu
 */
public class MoBaoAcctTradeQueryResponse extends MoBaoResponse {

    /**
     * 订单号
     */
    @NotEmpty
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 交易状态 1-交易成功 2-交易失败
     */
    @NotEmpty
    @XStreamAlias("TradeStatus")
    private String tradeStatus;

    /**
     * 交易金额
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    @XStreamAlias("CustParam")
    private String custParam;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String.format("MoBaoAcctTradeQueryResponse: {orderNo:%s, tradeStatus:%s, tradeAmt:%s, custParam:%s}",
                orderNo, tradeStatus, tradeAmt, custParam);
    }

}
