/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoDepositNotify extends MoBaoNotify {

    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo;

    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    @XStreamAlias("CustParam")
    private String custParam;

    /**
     * yyyyMMddHHmmss
     */
    @NotEmpty
    @XStreamAlias("TradeTime")
    private String tradeTime;

    @NotEmpty
    @XStreamAlias("BankCode")
    private String bankCode;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Override
    public String toString() {
        return "MoBaoDepositNotify [orderNo=" + orderNo + ", tradeAmt=" + tradeAmt + ", custParam=" + custParam
                + ", tradeTime=" + tradeTime + ", bankCode=" + bankCode + "]";
    }

}
