/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 4.8	投资撤销 响应报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayCancelResponse extends MoBaoResponse {

}
