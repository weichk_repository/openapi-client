/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 3.12 红包缴费查询 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoAcctTradeQueryRequest extends MoBaoRequest {

    /**
     * 订单号
     */
    @NotEmpty
    @XStreamAlias("OrderNo")
    private String orderNo;

    public MoBaoAcctTradeQueryRequest() {
        setMessageType("AcctTradeQuery");
    }

    public MoBaoAcctTradeQueryRequest(String orderNo) {
        this();
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return String.format("MoBaoAcctTradeQueryRequest: {orderNo:%s}", orderNo);
    }

}
