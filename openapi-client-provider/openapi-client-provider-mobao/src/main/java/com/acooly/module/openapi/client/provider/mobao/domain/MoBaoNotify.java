/**
 * create by zhangpu
 * date:2015年3月11日
 */
package com.acooly.module.openapi.client.provider.mobao.domain;

import com.acooly.module.openapi.client.api.message.DefaultApiMessage;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * @author zhangpu
 */
public class MoBaoNotify extends DefaultApiMessage {

    @XStreamAlias("MessageType")
    @XStreamAsAttribute
    private String messageType;

    public String getMessageType() {
        return messageType;
    }

    public void setMessageType(String messageType) {
        this.messageType = messageType;
    }

    @Override
    public String getService() {
        return this.messageType;
    }

}
