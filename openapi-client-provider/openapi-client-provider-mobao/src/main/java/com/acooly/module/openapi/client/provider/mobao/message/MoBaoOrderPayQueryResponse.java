/**
 * create by zhangpu
 * date:2015年3月25日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 4.4 用户投资查询 响应报文
 * 
 * @author zhangpu
 *
 */
public class MoBaoOrderPayQueryResponse extends MoBaoResponse {

	/***
	 * 投资流水号，需唯一，可以由字母和数字组成。
	 */
	@NotEmpty
	@XStreamAlias("TradeNo")
	private String TradeNo;

	/**
	 * 交易状态 1-已支付 2-已撤销
	 */
	@NotEmpty
	@XStreamAlias("TradeStatus")
	private String tradeStatus;

	/** 交易金额 */
	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	@XStreamAlias("CustParam")
	private String custParam;

	public String getTradeNo() {
		return TradeNo;
	}

	public void setTradeNo(String tradeNo) {
		TradeNo = tradeNo;
	}

	public String getTradeStatus() {
		return tradeStatus;
	}

	public void setTradeStatus(String tradeStatus) {
		this.tradeStatus = tradeStatus;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getCustParam() {
		return custParam;
	}

	public void setCustParam(String custParam) {
		this.custParam = custParam;
	}

	@Override
	public String toString() {
		return String.format("MoBaoOrderPayQueryResponse: {TradeNo:%s, tradeStatus:%s, tradeAmt:%s, custParam:%s}",
				TradeNo, tradeStatus, tradeAmt, custParam);
	}

}
