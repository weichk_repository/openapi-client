/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 4.5 标的查询 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderQueryRequest extends MoBaoRequest {

	/** 标的号，需唯一。可以由字母和数字组成 */
	@NotEmpty
	@XStreamAlias("OrderNo")
	private String orderNo;

	public MoBaoOrderQueryRequest() {
		setMessageType("OrderQuery");
	}

	public MoBaoOrderQueryRequest(String orderNo) {
		this();
		this.orderNo = orderNo;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	@Override
	public String toString() {
		return String.format("MoBaoOrderQueryRequest: {orderNo:%s}", orderNo);
	}

}
