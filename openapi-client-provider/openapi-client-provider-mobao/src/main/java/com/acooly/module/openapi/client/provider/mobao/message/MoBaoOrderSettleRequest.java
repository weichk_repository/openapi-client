/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.6 标的确认 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderSettleRequest extends MoBaoRequest {

    /**
     * 标的号，需唯一。可以由字母和数字组成
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 融资金额
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * 融资者ID
     */
    @NotEmpty
    @XStreamAlias("UserID")
    private String userId;

    /**
     * 风险准备金，可为0
     */
    @NotEmpty
    @XStreamAlias("RetainAmt")
    private String retainAmt = "0";

    /**
     * 佣金金额，可为0
     */
    @NotEmpty
    @XStreamAlias("Commission")
    private String commission;

    public MoBaoOrderSettleRequest() {
        setMessageType("OrderSettle");
    }

    public MoBaoOrderSettleRequest(String orderNo, String userId, String tradeAmt, String commission) {
        this();
        this.orderNo = orderNo;
        this.tradeAmt = tradeAmt;
        this.userId = userId;
        this.commission = commission;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getRetainAmt() {
        return retainAmt;
    }

    public void setRetainAmt(String retainAmt) {
        this.retainAmt = retainAmt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    @Override
    public String toString() {
        return String.format(
                "MoBaoOrderSettleRequest: {orderNo:%s, tradeAmt:%s, userId:%s, retainAmt:%s, commission:%s}", orderNo,
                tradeAmt, userId, retainAmt, commission);
    }


}
