/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 4.7	标的取消 响应报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderRepealResponse extends MoBaoResponse {

}
