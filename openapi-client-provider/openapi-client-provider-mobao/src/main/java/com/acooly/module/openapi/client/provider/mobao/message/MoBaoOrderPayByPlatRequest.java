/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.2 用户投资 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayByPlatRequest extends MoBaoRequest {

	/** 标的号，需唯一。可以由字母和数字组成 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("OrderNo")
	private String orderNo;

	/***
	 * 投资流水号，需唯一，可以由字母和数字组成。
	 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("TradeNo")
	private String TradeNo = Ids.getDid();

	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	@NotEmpty
	@XStreamAlias("BuyerID")
	private String buyerID;

	@NotEmpty
	@XStreamAlias("SellerID")
	private String sellerID;

	@NotEmpty
	@XStreamAlias("TradeSummary")
	private String tradeSummary = "签约投资付款";

	public MoBaoOrderPayByPlatRequest() {
		setMessageType("OrderPayByPlat");
	}

	public MoBaoOrderPayByPlatRequest(String orderNo, String buyerID, String sellerID, String tradeAmt) {
		this();
		this.orderNo = orderNo;
		this.tradeAmt = tradeAmt;
		this.buyerID = buyerID;
		this.sellerID = sellerID;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeNo() {
		return TradeNo;
	}

	public void setTradeNo(String tradeNo) {
		TradeNo = tradeNo;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getBuyerID() {
		return buyerID;
	}

	public void setBuyerID(String buyerID) {
		this.buyerID = buyerID;
	}

	public String getSellerID() {
		return sellerID;
	}

	public void setSellerID(String sellerID) {
		this.sellerID = sellerID;
	}

	public String getTradeSummary() {
		return tradeSummary;
	}

	public void setTradeSummary(String tradeSummary) {
		this.tradeSummary = tradeSummary;
	}

	@Override
	public String toString() {
		return String
				.format("MoBaoOrderPayByPlatRequest: {orderNo:%s, TradeNo:%s, tradeAmt:%s, buyerID:%s, sellerID:%s, tradeSummary:%s}",
						orderNo, TradeNo, tradeAmt, buyerID, sellerID, tradeSummary);
	}

}
