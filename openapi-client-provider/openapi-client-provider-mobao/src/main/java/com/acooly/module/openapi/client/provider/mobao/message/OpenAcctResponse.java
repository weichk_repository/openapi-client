/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 联机注册 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class OpenAcctResponse extends MoBaoResponse {

}
