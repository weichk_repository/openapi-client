/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.2 用户投资（前台） 请求报文
 * <p>
 * 跳转模式
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayRequest extends MoBaoRequest {

    /**
     * 标的号，需唯一。可以由字母和数字组成
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo = Ids.getDid();
    ;

    /***
     * 投资流水号，需唯一，可以由字母和数字组成。
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("tradeNo")
    private String TradeNo = Ids.getDid();

    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    @NotEmpty
    @XStreamAlias("BuyerID")
    private String buyerID;

    @NotEmpty
    @XStreamAlias("SellerID")
    private String sellerID;

    @XStreamAlias("CustParam")
    private String custParam;

    @NotEmpty
    @Size(max = 20)
    @XStreamAlias("CustomerIP")
    private String customerIP; // = Constants.SITE_IP;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("ReturnUrl")
    private String returnUrl; // = Constants.INVEST_RETRUE_URL;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("NotifyUrl")
    private String notifyUrl; // = Constants.NOTIFY_URL + "/OrderPay";

    @NotEmpty
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "投资付款";

    public MoBaoOrderPayRequest() {
        setMessageType("OrderPay");
    }

    public MoBaoOrderPayRequest(String buyerID, String sellerID, String tradeAmt) {
        super();
        this.tradeAmt = tradeAmt;
        this.buyerID = buyerID;
        this.sellerID = sellerID;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeNo() {
        return TradeNo;
    }

    public void setTradeNo(String tradeNo) {
        TradeNo = tradeNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getBuyerID() {
        return buyerID;
    }

    public void setBuyerID(String buyerID) {
        this.buyerID = buyerID;
    }

    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    public String getCustomerIP() {
        return customerIP;
    }

    public void setCustomerIP(String customerIP) {
        this.customerIP = customerIP;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoOrderPayRequest: {orderNo:%s, TradeNo:%s, tradeAmt:%s, buyerID:%s, sellerID:%s, custParam:%s, customerIP:%s, returnUrl:%s, notifyUrl:%s, tradeSummary:%s}",
                        orderNo, TradeNo, tradeAmt, buyerID, sellerID, custParam, customerIP, returnUrl, notifyUrl,
                        tradeSummary);
    }

}
