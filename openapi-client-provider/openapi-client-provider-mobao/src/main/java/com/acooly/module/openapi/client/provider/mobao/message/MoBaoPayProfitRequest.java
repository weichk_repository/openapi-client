/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 5.3 佣金分润 请求报文
 * <p>
 * 网贷平台发起佣金分润请求，将从融资者还款时收取的佣金中分润给推荐人虚拟账户（推荐人也必须是网贷平台的用户）
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoPayProfitRequest extends MoBaoRequest {

    /**
     * 佣金分润流水号，需唯一，可以由字母和数字组成
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("TradeNo")
    private String tradeNo;

    /**
     * 原标的号
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 分润总金额
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * 平台佣金金额
     */
    @NotEmpty
    @XStreamAlias("Commission")
    private String commission;

    /**
     * 还款人
     */
    @NotEmpty
    @XStreamAlias("SellerID")
    private String sellerID;

    /**
     * 交易摘要
     */
    @NotEmpty
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "分润";

    /**
     * 还款明细
     */
    @NotEmpty
    @XStreamAlias("BuyerList")
    private MoBaoBuyerList buyerList = new MoBaoBuyerList();

    @XStreamAlias("CustParam")
    private String custParam;

    public MoBaoPayProfitRequest() {
        setMessageType("PayProfit");
    }

    public MoBaoPayProfitRequest(String orderNo, String tradeNo, String tradeAmt, String commission, String sellerID) {
        this();
        this.orderNo = orderNo;
        this.tradeNo = tradeNo;
        this.tradeAmt = tradeAmt;
        this.commission = commission;
        this.sellerID = sellerID;
    }

    public void addBuyer(String buyerId, String amt) {
        buyerList.addBuyer(buyerId, amt);
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    public MoBaoBuyerList getBuyerList() {
        return buyerList;
    }

    public void setBuyerList(MoBaoBuyerList buyerList) {
        this.buyerList = buyerList;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoPayLoanRequest: {orderNo:%s, tradeNo:%s, tradeAmt:%s, commission:%s, sellerID:%s, tradeSummary:%s, buyerList:%s, custParam:%s}",
                        orderNo, tradeNo, tradeAmt, commission, sellerID, tradeSummary, buyerList, custParam);
    }

}
