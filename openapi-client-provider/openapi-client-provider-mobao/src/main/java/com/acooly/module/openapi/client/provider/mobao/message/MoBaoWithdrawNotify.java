/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoWithdrawNotify extends MoBaoNotify {

    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo;

    @NotEmpty
    @Size(max = 32)
    @XStreamAlias("UserID")
    private String userId;

    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * yyyyMMddHHmmss
     */
    @NotEmpty
    @XStreamAlias("TradeTime")
    private String tradeTime;

    /**
     * 提现状态： 1-银行卡打款成功 2-银行卡打款失败 4-提现申请成功
     */
    @NotEmpty
    @XStreamAlias("TradeStatus")
    private String tradeStatus;

    @XStreamAlias("CustParam")
    private String custParam;

    public String getTradeStatusLabel() {
        return tradeStatus.equals("1") ? "成功" : ("2".equals(tradeStatus) ? "失败" : "处理中");
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoWithdrawNotify: {orderNo:%s, userId:%s, tradeAmt:%s, tradeTime:%s, tradeStatus:%s, custParam:%s}",
                        orderNo, userId, tradeAmt, tradeTime, tradeStatus, custParam);
    }

}
