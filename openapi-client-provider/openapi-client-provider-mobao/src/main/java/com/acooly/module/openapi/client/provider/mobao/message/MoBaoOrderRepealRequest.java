/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.7 标的取消 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderRepealRequest extends MoBaoRequest {

	/** 标的号，需唯一。可以由字母和数字组成 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("OrderNo")
	private String orderNo;

	/** 融资金额 */
	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	/** 融资者ID */
	@NotEmpty
	@XStreamAlias("UserID")
	private String userId;

	public MoBaoOrderRepealRequest() {
		setMessageType("OrderRepeal");
	}

	public MoBaoOrderRepealRequest(String orderNo, String tradeAmt, String userId) {
		this();
		this.orderNo = orderNo;
		this.tradeAmt = tradeAmt;
		this.userId = userId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getUserId() {
		return userId;
	}

	@Override
	public String toString() {
		return String
				.format("MoBaoOrderRepealRequest: {orderNo:%s, tradeAmt:%s, userId:%s}", orderNo, tradeAmt, userId);
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

}
