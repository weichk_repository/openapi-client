/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 4.5 标的查询 响应报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderQueryResponse extends MoBaoResponse {
    /**
     * 标的号
     */
    @NotEmpty
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 服务器端交易号
     */
    @NotEmpty
    @XStreamAlias("OrderPayNo")
    private String orderPayNo;

    /**
     * 创建标日期 yyyyMMdd
     */
    @NotEmpty
    @XStreamAlias("OrderDate")
    private String orderDate;

    /**
     * 创建标时间 yyyyMMddhhmiss
     */
    @NotEmpty
    @XStreamAlias("TradeTime")
    private String tradeTime;

    /**
     * 交易状态：0-已创建 1-满标 2-已撤销
     */
    @NotEmpty
    @XStreamAlias("TradeStatus")
    private String tradeStatus;

    /**
     * 交易金额
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * 提留资金(商务合同规定的保证金，融资人不能提现的金额)
     */
    @NotEmpty
    @XStreamAlias("Commission")
    private String commission;

    /**
     * 融资服务费(平台收取融资人的佣金总和)
     */
    @NotEmpty
    @XStreamAlias("LoanServiceFee")
    private String loanServiceFee;

    /**
     * 已投金额
     */
    @NotEmpty
    @XStreamAlias("UseAmt")
    private String useAmt;

    @XStreamAlias("CustParam")
    private String custParam;

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getOrderPayNo() {
        return orderPayNo;
    }

    public void setOrderPayNo(String orderPayNo) {
        this.orderPayNo = orderPayNo;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public String getTradeTime() {
        return tradeTime;
    }

    public void setTradeTime(String tradeTime) {
        this.tradeTime = tradeTime;
    }

    public String getTradeStatus() {
        return tradeStatus;
    }

    public void setTradeStatus(String tradeStatus) {
        this.tradeStatus = tradeStatus;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getLoanServiceFee() {
        return loanServiceFee;
    }

    public void setLoanServiceFee(String loanServiceFee) {
        this.loanServiceFee = loanServiceFee;
    }

    public String getUseAmt() {
        return useAmt;
    }

    public void setUseAmt(String useAmt) {
        this.useAmt = useAmt;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoOrderQueryResponse: {orderNo:%s, orderPayNo:%s, orderDate:%s, tradeTime:%s, tradeStatus:%s, tradeAmt:%s, commission:%s, loanServiceFee:%s, useAmt:%s, custParam:%s}",
                        orderNo, orderPayNo, orderDate, tradeTime, tradeStatus, tradeAmt, commission, loanServiceFee,
                        useAmt, custParam);
    }

}
