/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 5.1 融资者账户还款 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoPayLoanRequest extends MoBaoRequest {

    /**
     * 标的号
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo;

    /**
     * 还款流水号。注意：这个第三方支付的服务器端交易流水是由客户端生成的. 基本意思是与客户端orderNo的概念合一
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("TradeNo")
    private String tradeNo;

    /**
     * 资人入账总金额 还款总金额=TradeAmt+ Commission
     * <p>
     * 表示困惑?应该是还款人出账=TradeAmt+ Commission?
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * 平台佣金金额
     */
    @NotEmpty
    @XStreamAlias("Commission")
    private String commission;

    /**
     * 还款人
     */
    @NotEmpty
    @XStreamAlias("SellerID")
    private String sellerID;

    /**
     * 交易摘要
     */
    @NotEmpty
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "还款";

    /**
     * 还款明细
     */
    @NotEmpty
    @XStreamAlias("BuyerList")
    private MoBaoBuyerList buyerList = new MoBaoBuyerList();

    @XStreamAlias("CustParam")
    private String custParam;

    public MoBaoPayLoanRequest() {
        setMessageType("PayLoan");
    }

    public MoBaoPayLoanRequest(String orderNo, String tradeNo, String tradeAmt, String commission, String sellerID) {
        this();
        this.orderNo = orderNo;
        this.tradeNo = tradeNo;
        this.tradeAmt = tradeAmt;
        this.commission = commission;
        this.sellerID = sellerID;
    }

    public void addBuyer(String buyerId, String amt) {
        buyerList.addBuyer(buyerId, amt);
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeNo() {
        return tradeNo;
    }

    public void setTradeNo(String tradeNo) {
        this.tradeNo = tradeNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getCommission() {
        return commission;
    }

    public void setCommission(String commission) {
        this.commission = commission;
    }

    public String getSellerID() {
        return sellerID;
    }

    public void setSellerID(String sellerID) {
        this.sellerID = sellerID;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    public MoBaoBuyerList getBuyerList() {
        return buyerList;
    }

    public void setBuyerList(MoBaoBuyerList buyerList) {
        this.buyerList = buyerList;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoPayLoanRequest: {orderNo:%s, tradeNo:%s, tradeAmt:%s, commission:%s, sellerID:%s, tradeSummary:%s, buyerList:%s, custParam:%s}",
                        orderNo, tradeNo, tradeAmt, commission, sellerID, tradeSummary, buyerList, custParam);
    }

}
