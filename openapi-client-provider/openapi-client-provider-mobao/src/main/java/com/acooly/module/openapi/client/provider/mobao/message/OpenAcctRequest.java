/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 联机注册 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class OpenAcctRequest extends MoBaoRequest {

    /**
     * 用户ID（用户名）
     */
    @NotEmpty
    @XStreamAlias("UserID")
    private String userId;

    /**
     * 真实姓名
     */
    @NotEmpty
    @XStreamAlias("UserName")
    private String userName;

    @NotEmpty
    @XStreamAlias("IDCard")
    private String idCard;

    @NotEmpty
    @XStreamAlias("MobileNo")
    private String mobileNo;

    @XStreamAlias("Email")
    private String email;

    public OpenAcctRequest() {
        setMessageType("OpenAcct");
    }

    public OpenAcctRequest(String userId, String userName, String idCard, String mobileNo) {
        this();
        this.userId = userId;
        this.userName = userName;
        this.idCard = idCard;
        this.mobileNo = mobileNo;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMobileNo() {
        return mobileNo;
    }

    public void setMobileNo(String mobileNo) {
        this.mobileNo = mobileNo;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }

}
