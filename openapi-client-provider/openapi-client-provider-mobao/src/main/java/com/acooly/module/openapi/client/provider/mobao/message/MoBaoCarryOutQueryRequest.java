/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * 3.8 充值提现查询 请求报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoCarryOutQueryRequest extends MoBaoRequest {

    /**
     * 标的号
     */
    @NotEmpty
    @XStreamAlias("OrderNo")
    private String orderNo = Ids.getDid();
    ;

    public MoBaoCarryOutQueryRequest() {
        setMessageType("CarryOutQuery");
    }

    public MoBaoCarryOutQueryRequest(String orderNo) {
        this();
        this.orderNo = orderNo;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    @Override
    public String toString() {
        return String.format("MoBaoCarryOutQueryRequest: {orderNo:%s}", orderNo);
    }

}
