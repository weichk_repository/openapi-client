/**
 * create by zhangpu
 * date:2015年3月2日
 */
package com.acooly.module.openapi.client.provider.mobao;

import com.acooly.module.openapi.client.api.AbstractApiServiceClient;
import com.acooly.module.openapi.client.api.marshal.ApiMarshal;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;

import java.util.Map;

/**
 * ApiService执行器
 *
 * @author zhangpu
 */
public class MoBaoApiServiceClient
        extends AbstractApiServiceClient<MoBaoRequest, MoBaoResponse, MoBaoNotify, MoBaoNotify> {


    private Transport transport;
    private ApiMarshal<String, MoBaoRequest> requestMarshal;
    private ApiMarshal<String, MoBaoRequest> redirectMarshal;
    private ApiUnmarshal<MoBaoResponse, String> responseUnmarshal;
    private ApiUnmarshal<MoBaoNotify, Map<String, String>> notifyUnmarshal;

    @Override
    protected ApiMarshal<String, MoBaoRequest> getRequestMarshal() {
        return this.requestMarshal;
    }

    @Override
    protected ApiUnmarshal<MoBaoResponse, String> getResponseUnmarshal() {
        return this.responseUnmarshal;
    }

    @Override
    protected ApiMarshal<String, MoBaoRequest> getRedirectMarshal() {
        return this.redirectMarshal;
    }

    @Override
    protected ApiUnmarshal<MoBaoNotify, Map<String, String>> getNoticeUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected ApiUnmarshal<MoBaoNotify, Map<String, String>> getReturnUnmarshal() {
        return this.notifyUnmarshal;
    }

    @Override
    protected Transport getTransport() {
        return this.transport;
    }


    @Override
    public String getName() {
        return MobaoConstants.PROVIDER_NAME;
    }
}
