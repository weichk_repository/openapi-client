/**
 * create by zhangpu
 * date:2015年3月22日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

/**
 * mobao 还款明细
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("Buyer")
public class MoBaoBuyer {

	/** 投资人 */
	@XStreamAlias("BuyerID")
	@XStreamAsAttribute
	private String buyerId;

	/** 还款金额 */
	@XStreamAlias("Amt")
	@XStreamAsAttribute
	private String amt;

	public MoBaoBuyer() {
		super();
	}

	public MoBaoBuyer(String buyerId, String amt) {
		super();
		this.buyerId = buyerId;
		this.amt = amt;
	}

	public String getBuyerId() {
		return buyerId;
	}

	public void setBuyerId(String buyerId) {
		this.buyerId = buyerId;
	}

	public String getAmt() {
		return amt;
	}

	public void setAmt(String amt) {
		this.amt = amt;
	}

	@Override
	public String toString() {
		return String.format("{buyerId:%s, amt:%s}", buyerId, amt);
	}

}
