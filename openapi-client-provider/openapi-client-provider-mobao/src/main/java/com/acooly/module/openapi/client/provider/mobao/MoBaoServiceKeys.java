/**
 * create by zhangpu
 * date:2015年3月24日
 */
package com.acooly.module.openapi.client.provider.mobao;

/**
 * @author zhangpu
 *
 */
public enum MoBaoServiceKeys {

	Deposit("WebCharge", "网银充值"),

	Withdraw("CarryOut", "跳转提现"),

	Sign("UserAuthPlatByPage", "跳转签约"),

	InvestQuery("OrderPayQuery", "投资单笔查询"),

	TenderQuery("OrderQuery", "借款标查询"),

	BonusQuery("AcctTradeQuery", "红包奖金查询"),

	CarryOutQuery("CarryOutQuery", "充提交易查询");

	private String key;
	private String val;

	private MoBaoServiceKeys(String key, String val) {
		this.key = key;
		this.val = val;
	}

	public String getKey() {
		return key;
	}

	public String getVal() {
		return val;
	}

}
