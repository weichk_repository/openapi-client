/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao;

import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.exception.ApiServerException;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.acooly.module.openapi.client.provider.mobao.message.*;
import org.apache.commons.lang3.StringUtils;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author zhangpu
 *
 */
public class MoBaoApiService {

	public static final String SUCCESS = "00";

	@Resource(name = "moBaoApiServiceClient")
	private ApiServiceClient<MoBaoRequest, MoBaoResponse, MoBaoNotify, MoBaoNotify> apiServiceClient;

	/**
	 * 开户注册
	 * 
	 * 无需激活，在用户首次充值时由第三方支付提示其激活
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse openAcct(OpenAcctRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/***
	 * 充值请求（生成充值跳转URL）
	 * 
	 * @param request
	 * @return
	 */
	public String depositRedirect(MoBaoDepositRequest request) {
		return apiServiceClient.redirectGet(request);
	}

	/**
	 * 充值同步返回报文解析
	 * 
	 * @param data
	 * @return
	 */
	public MoBaoDepositNotify depositReturn(Map<String, String> data) {
		return (MoBaoDepositNotify) apiServiceClient.notice(data, MoBaoServiceKeys.Deposit.getKey());
	}

	/**
	 * 请求提现URL
	 * 
	 * @param request
	 * @return
	 */
	public String withdrawRedirect(MoBaoWithdrawRequest request) {
		return apiServiceClient.redirectGet(request);
	}

	/**
	 * 提现同步返回报文解析
	 * 
	 * @param data
	 * @return
	 */
	public MoBaoWithdrawNotify withdrawReturn(Map<String, String> data) {
		return (MoBaoWithdrawNotify) apiServiceClient.notice(data, MoBaoServiceKeys.Withdraw.getKey());
	}

	/**
	 * 充值提现记录查询
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoCarryOutQueryResponse carryOutQuery(MoBaoCarryOutQueryRequest request) {
		return (MoBaoCarryOutQueryResponse) assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 签约Url
	 * 
	 * @param request
	 * @return
	 */
	public String signRedirect(MoBaoUserAuthPlatByPageRequest request) {
		return apiServiceClient.redirectGet(request);
	}

	/**
	 * 签约同步返回报文解析
	 * 
	 * @param data
	 * @return
	 */
	public MoBaoUserAuthPlatByPageNotify signReturn(Map<String, String> data) {
		return (MoBaoUserAuthPlatByPageNotify) apiServiceClient.notice(data, MoBaoServiceKeys.Sign.getKey());
	}

	/**
	 * 发放奖励或红包（现金模式）
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse acctChargeByPlat(MoBaoAcctChargeByPlatRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 红包奖励交易 单笔查询
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoAcctTradeQueryResponse acctTradeQuery(MoBaoAcctTradeQueryRequest request) {
		return (MoBaoAcctTradeQueryResponse) assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 创建借款标
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse createOrder(MoBaoCreateOrderRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 借款标确认(满标)
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse orderSettle(MoBaoOrderSettleRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 借款标取消(流标)
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse orderRepeal(MoBaoOrderRepealRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	public MoBaoOrderQueryResponse orderQuery(MoBaoOrderQueryRequest request) {
		return (MoBaoOrderQueryResponse) assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 请求投资URL
	 * 
	 * @param request
	 * @return
	 */
	public String investRedirect(MoBaoOrderPayRequest request) {
		return apiServiceClient.redirectGet(request);
	}

	/**
	 * 投资 同步接口（依赖签约）
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse orderPayByPlat(MoBaoOrderPayByPlatRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	public MoBaoOrderPayQueryResponse orderPayQuery(MoBaoOrderPayQueryRequest request) {
		return (MoBaoOrderPayQueryResponse) apiServiceClient.execute(request);
	}

	/**
	 * 取消投资
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse orderPayCancel(MoBaoOrderPayCancelRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 还款
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse payLoan(MoBaoPayLoanRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	/**
	 * 垫付
	 * 
	 * @param request
	 * @return
	 */
	public MoBaoResponse payRisk(MoBaoPayRiskRequest request) {
		return assertSuccess(apiServiceClient.execute(request));
	}

	protected MoBaoResponse assertSuccess(MoBaoResponse response) {
		if (!SUCCESS.equals(response.getRespCode())) {
			throw new ApiServerException(response.getRespCode(), response.getRespDesc()
					+ (StringUtils.isNotBlank(response.getRespDetail()) ? ":" + response.getRespDetail() : ""));
		}
		return response;
	}
}
