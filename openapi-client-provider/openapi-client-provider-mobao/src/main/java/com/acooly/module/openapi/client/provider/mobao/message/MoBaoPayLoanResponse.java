/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoResponse;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 5.1	融资者账户还款 响应报文
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoPayLoanResponse extends MoBaoResponse {

}
