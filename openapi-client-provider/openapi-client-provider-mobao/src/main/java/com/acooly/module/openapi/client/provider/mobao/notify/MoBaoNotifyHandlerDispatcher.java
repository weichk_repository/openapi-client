/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.provider.mobao.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;
import com.acooly.module.openapi.client.api.notify.AbstractSpringNotifyHandlerDispatcher;

import java.util.Map;

/**
 * mobao 支付网关异步通知分发器
 *
 * @author zhangpu
 * @date 2014年6月29日
 */
public class MoBaoNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

    @Override
    protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
        return Strings.substringAfterLast(notifyUrl, "/");
    }

    @Override
    protected ApiServiceClient getApiServiceClient() {
        return null;
    }
}
