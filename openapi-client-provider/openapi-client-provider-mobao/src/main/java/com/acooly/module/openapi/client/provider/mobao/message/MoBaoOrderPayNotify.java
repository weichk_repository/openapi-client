/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.provider.mobao.message;


import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoNotify;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoOrderPayNotify extends MoBaoNotify {

	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("tradeNo")
	private String TradeNo;

	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	@XStreamAlias("CustParam")
	private String custParam;

	/** yyyyMMddHHmmss */
	@NotEmpty
	@XStreamAlias("TradeTime")
	private String tradeTime;

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getCustParam() {
		return custParam;
	}

	public void setCustParam(String custParam) {
		this.custParam = custParam;
	}

	public String getTradeTime() {
		return tradeTime;
	}

	public void setTradeTime(String tradeTime) {
		this.tradeTime = tradeTime;
	}

	public String getTradeNo() {
		return TradeNo;
	}

	public void setTradeNo(String tradeNo) {
		TradeNo = tradeNo;
	}

	@Override
	public String toString() {
		return String.format("MoBaoOrderPayNotify: {TradeNo:%s, tradeAmt:%s, custParam:%s, tradeTime:%s}", TradeNo,
				tradeAmt, custParam, tradeTime);
	}

}
