/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.core.utils.Ids;
import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 3.9 红包奖励 请求报文
 * <p>
 * 由P2P平台系统直接向托管平台发起代发指令。 资金从平台红包账户转入到用户账户中。
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoAcctChargeByPlatRequest extends MoBaoRequest {

    /**
     * 订单号，需唯一。可以由字母和数字组成
     */
    @NotEmpty
    @Size(max = 30)
    @XStreamAlias("OrderNo")
    private String orderNo = Ids.getDid();

    /**
     * 金额
     */
    @NotEmpty
    @XStreamAlias("TradeAmt")
    private String tradeAmt;

    /**
     * 用户ID
     */
    @XStreamAlias("UserID")
    private String userId;

    @NotEmpty
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "奖励";

    @XStreamAlias("CustParam")
    private String custParam;

    public MoBaoAcctChargeByPlatRequest() {
        setMessageType("AcctChargeByPlat");
    }

    public MoBaoAcctChargeByPlatRequest(String userId, String tradeAmt) {
        this();
        this.tradeAmt = tradeAmt;
        this.userId = userId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public String getTradeAmt() {
        return tradeAmt;
    }

    public void setTradeAmt(String tradeAmt) {
        this.tradeAmt = tradeAmt;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    @Override
    public String toString() {
        return String.format(
                "MoBaoAcctChargeByPlatRequest: {orderNo:%s, tradeAmt:%s, userId:%s, tradeSummary:%s, custParam:%s}",
                orderNo, tradeAmt, userId, tradeSummary, custParam);
    }


}
