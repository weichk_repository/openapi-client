/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-19 18:12 创建
 */
package com.acooly.module.openapi.client.provider.mobao;

/**
 * @author zhangpu 2017-09-19 18:12
 */
public class MobaoConstants {

    public static final String PROVIDER_NAME = "mobao";

}
