/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 4.1 融资标的创建 请求报文
 * 
 * @author zhangpu
 *
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoCreateOrderRequest extends MoBaoRequest {

	/** 标的号，需唯一。可以由字母和数字组成 */
	@NotEmpty
	@Size(max = 30)
	@XStreamAlias("OrderNo")
	private String orderNo;

	@NotEmpty
	@XStreamAlias("TradeAmt")
	private String tradeAmt;

	@NotEmpty
	@XStreamAlias("UserID")
	private String userId;

	@NotEmpty
	@XStreamAlias("TradeSummary")
	private String tradeSummary = "创建标";

	@XStreamAlias("CustParam")
	private String custParam;

	public MoBaoCreateOrderRequest() {
		setMessageType("CreateOrder");
	}

	public MoBaoCreateOrderRequest(String orderNo, String tradeAmt, String userId) {
		this();
		this.orderNo = orderNo;
		this.tradeAmt = tradeAmt;
		this.userId = userId;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public String getTradeAmt() {
		return tradeAmt;
	}

	public void setTradeAmt(String tradeAmt) {
		this.tradeAmt = tradeAmt;
	}

	public String getTradeSummary() {
		return tradeSummary;
	}

	public void setTradeSummary(String tradeSummary) {
		this.tradeSummary = tradeSummary;
	}

	public String getCustParam() {
		return custParam;
	}

	public void setCustParam(String custParam) {
		this.custParam = custParam;
	}

}
