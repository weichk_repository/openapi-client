/**
 * create by zhangpu
 * date:2015年3月19日
 */
package com.acooly.module.openapi.client.provider.mobao.message;

import com.acooly.module.openapi.client.provider.mobao.domain.MoBaoRequest;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 2.1 授权签约 请求报文
 * <p>
 * 跳转请求
 *
 * @author zhangpu
 */
@XStreamAlias("MoBaoAccount")
public class MoBaoUserAuthPlatByPageRequest extends MoBaoRequest {

    @NotEmpty
    @Size(max = 32)
    @XStreamAlias("UserID")
    private String userId;

    @XStreamAlias("CustParam")
    private String custParam;

    @NotEmpty
    @Size(max = 20)
    @XStreamAlias("CustomerIP")
    private String customerIP; // = Constants.SITE_IP;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("ReturnUrl")
    private String returnUrl; // = Constants.SGIN_RETRUE_URL;

    @NotEmpty
    @Size(max = 128)
    @XStreamAlias("NotifyUrl")
    private String notifyUrl; // = Constants.NOTIFY_URL + "/UserAuthPlatByPage";

    @Size(max = 120)
    @XStreamAlias("TradeSummary")
    private String tradeSummary = "投资签约";

    public MoBaoUserAuthPlatByPageRequest() {
        setMessageType("UserAuthPlatByPage");
    }

    public MoBaoUserAuthPlatByPageRequest(String userId) {
        this();
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getCustParam() {
        return custParam;
    }

    public void setCustParam(String custParam) {
        this.custParam = custParam;
    }

    public String getCustomerIP() {
        return customerIP;
    }

    public void setCustomerIP(String customerIP) {
        this.customerIP = customerIP;
    }

    public String getReturnUrl() {
        return returnUrl;
    }

    public void setReturnUrl(String returnUrl) {
        this.returnUrl = returnUrl;
    }

    public String getNotifyUrl() {
        return notifyUrl;
    }

    public void setNotifyUrl(String notifyUrl) {
        this.notifyUrl = notifyUrl;
    }

    public String getTradeSummary() {
        return tradeSummary;
    }

    public void setTradeSummary(String tradeSummary) {
        this.tradeSummary = tradeSummary;
    }

    @Override
    public String toString() {
        return String
                .format("MoBaoUserAuthPlatByPageRequest: {userId:%s, custParam:%s, customerIP:%s, returnUrl:%s, notifyUrl:%s, tradeSummary:%s}",
                        userId, custParam, customerIP, returnUrl, notifyUrl, tradeSummary);
    }

}
