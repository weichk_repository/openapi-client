package com.acooly.module.openapi.client.provider.mobao;

import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.provider.mobao.OpenAPIClientMobaoProperties.PREFIX;

@EnableConfigurationProperties({OpenAPIClientMobaoProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
public class OpenAPIClientMobaoConfigration {

    @Autowired
    private OpenAPIClientMobaoProperties openAPIClientMobaoProperties;

    @Bean("mobaoHttpTransport")
    public Transport fuiouHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setGateway(openAPIClientMobaoProperties.getGateway());
        httpTransport.setConnTimeout(String.valueOf(openAPIClientMobaoProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientMobaoProperties.getReadTimeout()));
        return httpTransport;
    }
}
