package com.acooly.module.openapi.client.api.exception;

/**
 * SocketTimeoutException 读超时异常  通常业务端需要针对该异常做特殊处理
 * Created by liubin@prosysoft.com on 2017/10/10.
 */
public class ApiClientSocketTimeoutException extends ApiClientNetworkException {
	
	public ApiClientSocketTimeoutException () {
		super();
	}
	
	public ApiClientSocketTimeoutException (String message, Throwable cause) {
		super (message, cause);
	}
	
	public ApiClientSocketTimeoutException (String message) {
		super (message);
	}
	
	public ApiClientSocketTimeoutException (Throwable cause) {
		super (cause);
	}
}
