/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.api.notify;

import com.acooly.module.openapi.client.api.message.ApiMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 默认通知处理接口
 * 
 * 只打印通知结果 商户系统可根据业务情况开发自己的通知处理器实现.
 * 
 * @author zhangpu
 * @date 2014年6月16日
 */
public class DefaultNotifyHandler implements NotifyHandler {
	private static Logger logger = LoggerFactory.getLogger(DefaultNotifyHandler.class);

	@Override
	public void handleNotify(ApiMessage notify) {
		logger.info("异步通知 打印：" + notify);
	}

	@Override
	public String serviceKey() {
		return "default";
	}

}
