/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.api.util;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.naming.NoNameCoder;
import com.thoughtworks.xstream.io.xml.CompactWriter;
import com.thoughtworks.xstream.io.xml.DomDriver;

import java.io.StringWriter;

/**
 * @author zhangpu
 *
 */
public class XStreams {

	private static XStream xstream = new XStream((new DomDriver("UTF-8", new NoNameCoder())));
	static {
		// 指定所有class均解析annotations
		xstream.autodetectAnnotations(true);
	}

	public static XStream getXStream() {
		return xstream;
	}

	public static String toXmlCompact(Object object) {
		StringWriter sw = new StringWriter();
		xstream.marshal(object, new CompactWriter(sw));
		return sw.toString();
	}

	public static String toXml(Object object) {
		return xstream.toXML(object);
	}

	public static Object fromXml(String xml, Object root) {
		return xstream.fromXML(xml, root);
	}

}
