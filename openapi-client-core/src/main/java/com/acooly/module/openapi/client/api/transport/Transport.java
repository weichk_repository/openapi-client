package com.acooly.module.openapi.client.api.transport;

import com.acooly.core.utils.net.HttpResult;

import java.util.Map;

public interface Transport {


    HttpResult request(String message, String url, Map<String, String> headers);

    HttpResult request(String message, String url);

    HttpResult request(String message);

    /**
     * 发送请求，接收响应
     *
     * @param message
     * @param url
     * @return
     */
    String exchange(String message, String url);

    String exchange(String message);
}
