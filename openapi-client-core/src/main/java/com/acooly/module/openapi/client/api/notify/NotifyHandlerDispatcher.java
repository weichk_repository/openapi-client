/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.api.notify;

import java.util.Map;

/**
 * @author zhangpu
 *
 */
public interface NotifyHandlerDispatcher {

	void dispatch(String notifyUrl, Map<String, String> notifyData);

}
