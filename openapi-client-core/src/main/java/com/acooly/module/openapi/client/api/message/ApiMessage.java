/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.api.message;

/**
 * @author zhangpu
 *
 */
public interface ApiMessage {

	String getService();

	String getPartner();

}
