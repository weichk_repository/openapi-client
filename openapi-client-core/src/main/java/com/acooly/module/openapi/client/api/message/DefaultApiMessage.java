/**
 * create by zhangpu
 * date:2015年4月11日
 */
package com.acooly.module.openapi.client.api.message;

/**
 * @author zhangpu
 *
 */
public class DefaultApiMessage implements ApiMessage {

	private String service;
	private String partner;

	@Override
	public String getService() {
		return this.service;
	}

	@Override
	public String getPartner() {
		return this.partner;
	}

	public void setService(String service) {
		this.service = service;
	}

	public void setPartner(String partner) {
		this.partner = partner;
	}

	@Override
	public String toString() {
		return String.format("DefaultApiMessage: {service:%s, partner:%s}", service, partner);
	}

}
