/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月22日
 *
 */
package com.acooly.module.openapi.client.api.secrity;

/**
 * 加解密处理 接口
 * 
 * @author zhangpu
 */
public interface CryptoHandler {

	/**
	 * 加密
	 * 
	 * @param plain
	 */
	String encrypt(String plain);

	/**
	 * 解密
	 * 
	 * @param crypt
	 */
	String decrypt(String crypt);

}
