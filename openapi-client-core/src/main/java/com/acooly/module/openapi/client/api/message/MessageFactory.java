/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.api.message;

/**
 * 报文实例工厂
 * 
 * @author zhangpu
 * @date 2014年6月13日
 */
public interface MessageFactory {
	
	ApiMessage getRequest(String serviceName);
	
	ApiMessage getResponse(String serviceName);

	ApiMessage getNotify(String serviceName);

	ApiMessage getReturn(String serviceName);
}
