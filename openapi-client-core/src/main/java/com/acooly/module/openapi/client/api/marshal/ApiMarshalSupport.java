/**
 * create by zhangpu
 * date:2015年4月15日
 */
package com.acooly.module.openapi.client.api.marshal;

import com.acooly.module.safety.signature.SignTypeEnum;
import com.acooly.module.safety.signature.Signer;
import com.acooly.module.safety.signature.SignerFactory;

/**
 * @author zhangpu
 */
public abstract class ApiMarshalSupport {

    private SignerFactory signerFactory;

    public void setSignerFactory(SignerFactory signerFactory) {
        this.signerFactory = signerFactory;
    }

    public SignerFactory getSignerFactory() {
        return signerFactory;
    }

    protected Signer getSigner(SignTypeEnum signType) {
        return signerFactory.getSigner(signType.name());
    }

}
