/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.api.anotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author zhangpu
 */
@Target({ ElementType.ANNOTATION_TYPE, ElementType.FIELD, ElementType.METHOD, ElementType.PARAMETER })
@Retention(RetentionPolicy.RUNTIME)
public @interface ApiItem {
	/**
	 * 是否参与签名 默认:true
	 * @return
	 */
	public boolean sign() default true;

	/**
	 * 别名定义
	 * @return
	 */
	public String value() default "";
	
	/**
	 * 是否加密/解密 默认:false
	 * @return
	 */
	public boolean securet() default false;
}
