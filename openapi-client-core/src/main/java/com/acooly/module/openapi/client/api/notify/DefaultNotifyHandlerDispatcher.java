/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年4月1日
 *
 */
package com.acooly.module.openapi.client.api.notify;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.ApiServiceClient;

import java.util.Map;

/**
 * @author zhangpu
 */
public class DefaultNotifyHandlerDispatcher extends AbstractSpringNotifyHandlerDispatcher {

	@Override
	protected String getServiceKey(String notifyUrl, Map<String, String> notifyData) {
		return Strings.substringAfterLast(notifyUrl, "/");
	}


	@Override
	protected ApiServiceClient getApiServiceClient() {
		return null;
	}
}
