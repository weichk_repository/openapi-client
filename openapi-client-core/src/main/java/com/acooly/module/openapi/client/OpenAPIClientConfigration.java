package com.acooly.module.openapi.client;

import com.acooly.module.safety.SafetyAutoConfig;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.ComponentScan;

import static com.acooly.module.openapi.client.OpenAPIClientProperties.PREFIX;


/**
 */
@EnableConfigurationProperties({OpenAPIClientProperties.class})
@ConditionalOnProperty(value = PREFIX + ".enable", matchIfMissing = true)
@AutoConfigureAfter(SafetyAutoConfig.class)
@ComponentScan("com.acooly.module.openapi.client.api")
public class OpenAPIClientConfigration {


}
