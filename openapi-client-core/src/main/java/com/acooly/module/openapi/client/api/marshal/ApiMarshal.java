/**
 * create by zhangpu
 * date:2015年3月20日
 */
package com.acooly.module.openapi.client.api.marshal;

/**
 * @author zhangpu
 *
 */
public interface ApiMarshal<T, S> {

	T marshal(S source);
}
