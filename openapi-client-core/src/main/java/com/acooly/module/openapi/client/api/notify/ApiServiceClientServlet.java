/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.api.notify;

import com.acooly.core.common.web.servlet.AbstractSpringServlet;
import com.acooly.core.utils.Servlets;
import com.acooly.core.utils.Strings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhangpu
 * @date 2014年6月16日
 */
public class ApiServiceClientServlet extends AbstractSpringServlet {
    /**
     * UID
     */
    private static final long serialVersionUID = 6169228472722574634L;

    private NotifyHandlerDispatcher notifyHandlerDispatcher;

    private static final int SUCCESS_RESPONSE_CODE = 200;
    private static final String SUCCESS_RESPONSE_BODY = "SUCCESS";
    private static final String DEFAULT_NOTIFY_DISPATCHER_BEAN_NAME = "clientNotifyHandlerDispatcher";

    public static final String SUCCESS_RESPONSE_CODE_KEY = "success_response_code";
    public static final String SUCCESS_RESPONSE_BODY_KEY = "success_response_body";
    public static final String NOTIFY_DISPATCHER_BEAN_NAME_KEY = "NOTIFY_DISPATCHER_BEAN_NAME_KEY";

    @Override
    protected void doInit() {
        super.doInit();
        notifyHandlerDispatcher = getBean(getDispatcherBeanName(), NotifyHandlerDispatcher.class);
    }

    @Override
    protected void doService(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Map<String, Object> received = Servlets.getParametersStartingWith(request, null);
            Map<String, String> notifyData = new HashMap<String, String>(received.size());
            for (Map.Entry<String, Object> entry : received.entrySet()) {
                notifyData.put(entry.getKey(), (String) entry.getValue());
            }
            String notifyUrl = Servlets.getRequestPath(request);
            notifyHandlerDispatcher.dispatch(notifyUrl, notifyData);
            response.setStatus(getSuccessResponseCode());
            Servlets.writeResponse(response, getSuccessResponseBody(), null);
        } catch (Exception e) {
            Servlets.writeResponse(response, "failure", null);
        }
    }

    protected String getDispatcherBeanName() {
        String beanName = getInitParameter(NOTIFY_DISPATCHER_BEAN_NAME_KEY);
        if (Strings.isBlank(beanName)) {
            beanName = DEFAULT_NOTIFY_DISPATCHER_BEAN_NAME;
        }
        return beanName;
    }

    protected String getSuccessResponseBody() {
        String body = getInitParameter(SUCCESS_RESPONSE_BODY_KEY);
        if (Strings.isBlank(body)) {
            body = SUCCESS_RESPONSE_BODY;
        }
        return body;
    }

    protected int getSuccessResponseCode() {
        try {
            return Integer.parseInt(getInitParameter(SUCCESS_RESPONSE_CODE_KEY));
        } catch (Exception e) {
            return SUCCESS_RESPONSE_CODE;
        }
    }

}
