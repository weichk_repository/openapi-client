/**
 * create by zhangpu
 * date:2015年4月12日
 */
package com.acooly.module.openapi.client.api.notify;

/**
 * @author zhangpu
 *
 */
public interface NotifyHandlerFactory {

	NotifyHandler getNotifyHandler(String serviceKey);

}
