package com.acooly.module.openapi.client.api.exception;

/**
 * openApi 处理中异常,通常业务接收到此异常，应该当初处理中来处理这笔业务
 * 
 * @author zhike
 * @date 2018年9月5日
 */
public class ApiClientProcessingException extends RuntimeException {

	/** UID */
	private static final long serialVersionUID = 788548256305224364L;

	public ApiClientProcessingException() {
		super();
	}

	/**
	 * @param message
	 * @param cause
	 */
	public ApiClientProcessingException(String message, Throwable cause) {
		super(message, cause);
	}

	/**
	 * @param message
	 */
	public ApiClientProcessingException(String message) {
		super(message);
	}

	/**
	 * @param cause
	 */
	public ApiClientProcessingException(Throwable cause) {
		super(cause);
	}

}
