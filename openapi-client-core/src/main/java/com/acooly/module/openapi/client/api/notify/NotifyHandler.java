/**
 * coding by zhangpu
 */
package com.acooly.module.openapi.client.api.notify;

import com.acooly.module.openapi.client.api.message.ApiMessage;

/**
 * @author zhangpu
 * @date 2014年6月16日
 */
public interface NotifyHandler {

	void handleNotify(ApiMessage notify);

	String serviceKey();

}
