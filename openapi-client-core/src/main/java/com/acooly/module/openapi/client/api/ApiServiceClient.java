/**
 * create by zhangpu
 * date:2015年3月21日
 */
package com.acooly.module.openapi.client.api;

import com.acooly.core.utils.lang.Named;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.api.message.PostRedirect;

import java.util.Map;

/**
 * Api SDK 客户端执行接口
 *
 * @author zhangpu
 */
public interface ApiServiceClient<R extends ApiMessage, T extends ApiMessage, N extends ApiMessage, X extends ApiMessage> extends Named {

    /**
     * 同步接口调用
     *
     * @param request
     * @return
     */
    T execute(R request);

    /**
     * 跳转接口生成跳转报文和URL(GET)
     *
     * @param request
     * @return
     */
    String redirectGet(R request);

    /**
     * 跳转接口生成报文和URL(POST)
     *
     * @param request
     * @return
     */
    PostRedirect redirectPost(R request);

    /**
     * 解析异步通知
     *
     * @param notifyData
     * @param serviceKey
     * @return
     */
    N notice(Map<String, String> notifyData, String serviceKey);

    /**
     * 解析同步通知
     *
     * @param notifyData
     * @param serviceKey
     * @return
     */
    X result(Map<String, String> notifyData, String serviceKey);

}
