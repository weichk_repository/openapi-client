package com.acooly.module.openapi.client;

import org.springframework.boot.SpringApplication;

import com.acooly.core.common.BootApp;
import com.acooly.core.common.boot.Apps;

@BootApp(sysName = "openapi-client", httpPort = 8384)
public class Main {
	public static void main(String[] args) {
		Apps.setProfileIfNotExists("sdev");
		new SpringApplication(Main.class).run(args);
	}

}
