/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 17:10 创建
 */
package com.acooly.module.openapi.client.file.domain;

import com.acooly.core.utils.ToString;
import com.google.common.collect.Maps;

import java.util.Map;

/**
 * @author zhangpu 2018-02-10 17:10
 */
public class FileHandlerOrder {

    private String period;

    private String name;

    private Map<String, Object> exts = Maps.newHashMap();


    public void put(String key, Object val) {
        exts.put(key, val);
    }

    public Object get(String key) {
        return exts.get(key);
    }

    public FileHandlerOrder() {
    }

    public FileHandlerOrder(String period, String name) {
        this.period = period;
        this.name = name;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
