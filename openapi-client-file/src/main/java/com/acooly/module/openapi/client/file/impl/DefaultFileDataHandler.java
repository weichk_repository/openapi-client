/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-27 15:25 创建
 */
package com.acooly.module.openapi.client.file.impl;

import lombok.extern.slf4j.Slf4j;

import java.util.List;

/**
 * 文件数据处理默认实现
 * <p>
 * 工厂通过spring容器内如果默认找不到实现，则使用默认实现
 *
 * @author zhangpu 2018-02-27 15:25
 */
@Slf4j
public class DefaultFileDataHandler extends AbstractFileDataHandler {

    public static final String DEFAULT_FILE_DATA_HANDLER_NAME = "DEFAULT_FILE_DATA_HANDLER_NAME";

    @Override
    public void handle(String period, int index, List<String> rows, boolean finished) {
        log.info("period:{}, index:{}, finished:{}, rows count:{}", period, index, finished, rows.size());
    }

    @Override
    public String name() {
        return DEFAULT_FILE_DATA_HANDLER_NAME;
    }
}
