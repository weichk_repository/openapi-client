/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 17:10 创建
 */
package com.acooly.module.openapi.client.file;

import com.acooly.module.openapi.client.file.domain.FileHandlerOrder;

/**
 * @author zhangpu 2018-02-10 17:10
 */
public interface FileHandler {

    /**
     * 文件下载和数据处理
     * <p>
     * 数据处理：传入文件数据处理器（FileDataHandler），如果传入空，则框架扫描FileDataHandler实现，通过fileType匹配
     *
     * @param order           请求参数
     * @param fileDataHandler 文件数据处理器
     */
    void downloadHandle(FileHandlerOrder order, FileDataHandler fileDataHandler);

    /**
     * 文件下载和数据处理
     * <p>
     * 数据处理：框架扫描FileDataHandler实现，通过fileType匹配
     *
     * @param order 请求参数
     */
    void downloadHandle(FileHandlerOrder order);


    /**
     * 文件上传和数据处理
     *
     *
     * @param order
     */
    //todo: 文件组装和上传接口和实现，待设计优化
    void uploadHandle(FileHandlerOrder order);

}
