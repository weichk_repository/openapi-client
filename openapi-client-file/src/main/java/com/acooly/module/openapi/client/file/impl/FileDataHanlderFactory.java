/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-27 15:12 创建
 */
package com.acooly.module.openapi.client.file.impl;

import com.acooly.core.common.boot.Apps;
import com.acooly.module.openapi.client.file.FileDataHandler;
import com.google.common.collect.Maps;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * 文件處理器工廠
 *
 * @author zhangpu 2018-02-27 15:12
 */
@Slf4j
@Component
public class FileDataHanlderFactory implements InitializingBean {


    private Map<String, FileDataHandler> containers = Maps.newHashMap();


    public FileDataHandler getFileDataHandler(String name) {
        FileDataHandler fileDataHandler = containers.get(name);
        if (fileDataHandler == null) {
            throw new RuntimeException("没有对应的文件处理实现. name:" + name);
        }
        return fileDataHandler;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, FileDataHandler> beans = Apps.getApplicationContext().getBeansOfType(FileDataHandler.class);
        for (FileDataHandler fileDataHandler : beans.values()) {
            containers.put(fileDataHandler.name(), fileDataHandler);
        }
    }
}
