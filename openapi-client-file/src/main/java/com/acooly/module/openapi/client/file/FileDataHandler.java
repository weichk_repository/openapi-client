/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 17:04 创建
 */
package com.acooly.module.openapi.client.file;

import java.util.List;

/**
 * 文件处理接口
 *
 * @author zhangpu 2018-02-10 17:04
 */
public interface FileDataHandler {

    /**
     * 批次处理
     *
     * @param period   账期
     * @param index    页码
     * @param rows     数据
     * @param finished 是否结束
     */
    void handle(String period, int index, List<String> rows, boolean finished);

    /**
     * 批次大小
     *
     * @return 批次大小
     */
    int getBatchSize();

    /**
     * 对账文件类型(唯一标识)
     *
     * @return 文件唯一标识
     */
    String name();

}
