/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 17:12 创建
 */
package com.acooly.module.openapi.client.file.impl;

import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.file.FileDataHandler;
import com.acooly.module.openapi.client.file.FileHandler;
import com.acooly.module.openapi.client.file.domain.FileHandlerOrder;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author zhangpu 2018-02-10 17:12
 */
@Slf4j
public abstract class AbstractFileHandler implements FileHandler {


    private String colSplit = "|";
    private String fileCharset = "GBK";
    @Autowired
    protected FileDataHanlderFactory fileDataHanlderFactory;

    @Override
    public void downloadHandle(FileHandlerOrder order, FileDataHandler fileDataHandler) {
        log.info("文件处理 入参. order:{},fileDataHandler:{}", order, fileDataHandler);
        try {
            File file = doDownload(order);
            log.info("文件处理 文件：{}", file.getPath());
            doHandleFile(file, order, fileDataHandler);
        } catch (Exception e) {
            log.error("文件处理 失败:{}", e.getMessage());
            throw new RuntimeException("文件处理 失败", e);
        }
        log.info("文件处理 完成. order:{},fileDataHandler:{}", order, fileDataHandler);
    }


    @Override
    public void downloadHandle(FileHandlerOrder order) {
        if (fileDataHanlderFactory == null) {
            throw new RuntimeException("请在Spring容器内使用该方法");
        }
        downloadHandle(order, fileDataHanlderFactory.getFileDataHandler(order.getName()));
    }


    /**
     * 具体下载文件实现
     *
     * @param order
     * @return
     */
    public abstract File doDownload(FileHandlerOrder order);


    protected void doHandleFile(File file, FileHandlerOrder order, FileDataHandler fileDataHandler) {
        int batchSize = fileDataHandler.getBatchSize();
        if (batchSize <= 0) {
            throw new RuntimeException("批量大小不能设置为小于0");
        }
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new InputStreamReader(new FileInputStream(file), "GBK"));
            String line = null;
            int count = 0;
            int index = 1;
            List<String> buffer = Lists.newArrayList();
            while ((line = reader.readLine()) != null) {
                if (Strings.isBlank(line)) {
                    continue;
                }
                count = count + 1;
                if (buffer.size() == batchSize) {
                    fileDataHandler.handle(order.getPeriod(), index, buffer, false);
                    buffer.clear();
                    index = index + 1;
                }
                buffer.add(line);
            }
            if (buffer.size() > 0) {
                fileDataHandler.handle(order.getPeriod(), index, buffer, true);
            }
            log.info("文件处理 数据解析处理 完成. size:{}", count);
        } catch (Exception e) {
            log.error("文件处理 数据解析处理 失败:{}", e.getMessage());
            throw new RuntimeException("文件处理 数据解析处理", e);
        } finally {
            IOUtils.closeQuietly(reader);
        }
    }


    // todo:待实现
    @Override
    public void uploadHandle(FileHandlerOrder order) {

    }
}
