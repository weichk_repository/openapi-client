/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-10 17:06 创建
 */
package com.acooly.module.openapi.client.file.impl;

import com.acooly.module.openapi.client.file.FileDataHandler;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * @author zhangpu 2018-02-10 17:06
 */
@Slf4j
public abstract class AbstractFileDataHandler implements FileDataHandler {

    private static final int FILE_HANDLE_BATCH_SIZE = 100;

    private int batchSize = FILE_HANDLE_BATCH_SIZE;

    @Override
    public int getBatchSize() {
        return this.batchSize;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("name", this.name())
                .append("batchSize", batchSize)
                .toString();
    }
}
